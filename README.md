# chart

A npm package containing a web component rendering different types of charts build on top of chart.js.

See [chartjs.org](https://www.chartjs.org/) for more information.

## Examples of Web Components frameworks
- [stencil.js](https://stenciljs.com/)
- [Polymer](https://www.polymer-project.org/)
- [Svelte](https://svelte.dev/)
- [LitElement](https://lit-element.polymer-project.org/)

## Additional resources
- [Web Components - MDN](https://developer.mozilla.org/en-US/docs/Web/Web_Components)
- [The power of Web Components](https://hacks.mozilla.org/2018/11/the-power-of-web-components/)

## Some links on how to use web components within frameworks (Angular)
- [Web Components and Angular](https://coryrylan.com/blog/using-web-components-in-angular)
- [Web Components within angular forms - data-bindings...](https://coryrylan.com/blog/using-web-components-in-angular-forms)