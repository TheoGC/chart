class WcComponent extends HTMLElement {
  /*
    An instance of the element is created.
    Useful for initializing state, setting up event listeners, or creating a shadow dom.
    See the spec for restrictions on what you can do in the constructor.
  */
  constructor() {
    super();
  }

  /*
    Called every time the element is inserted into the DOM.
    Useful for running setup code, such as fetching resources or rendering.
    Generally, you should try to delay work until this time.
  */
  connectedCallback() {}

  /*
    Called every time the element is removed from the DOM. Useful for running clean up code.
  */
  disconnectedCallback() {}

  /*
    The custom element has been moved into a new document (e.g. someone called document.adoptNode(el)).
  */
  adoptedCallback() {}

  /*
    Called when an observed attribute has been added, removed, updated, or replaced.
    Also called for initial values when an element is created by the parser, or upgraded.
    Note: only attributes listed in the observedAttributes property will receive this callback.
  */
  attributeChangedCallback() {}
}

customElements.define('wc-component', WcComponent);
