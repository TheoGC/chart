import Chart from 'chart.js';

const template = document.createElement('template');
template.innerHTML = `
  <style>
    :host {
      all: initial;
      display: block;
      contain: content;
    }
    :host([hidden]) {
      display: none;
    }
  </style>
  <canvas></canvas>
`;

class WcChart extends HTMLElement {
  constructor() {
    super();

    const shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.appendChild(template.content.cloneNode(true));

    this._context = shadowRoot.querySelector('canvas').getContext('2d');

    this._config = null;
    this._chart = null;
  }

  setConfig({ type, options }) {
    this._config = { type, options };
  }

  setData(data) {
    if (!this._config) {
      console.info('The config of the component need to be initialised first');
      return;
    }

    this._chart = new Chart(this._context, {
      type: this._config.type,
      options: this._config.options,
      data
    });
  }

  toggleLegend(boolean) {
    this._chart.options.legend.display = boolean;
    this._chart.update();
  }

  set title(title) {
    this._chart.options.title.display = true;
    this._chart.options.title.text = title;
    this._chart.update();
  }
}

customElements.define('wc-chart', WcChart);

export default WcChart;
